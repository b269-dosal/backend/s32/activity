
let webS = require("http");
const port = 4000;

webS.createServer(function (req, res){
	//No Specific Method
	if(req.url === "/")
	{
		res.writeHead(200,{"Content-Type": "text/plain"});
		res.end("Welcome to Booking System");
	}; 

	if(req.url === "/profile" && req.method === "GET")
	{
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to your profile!");
	}; 


	if(req.url === "/courses" && req.method === "POST")
	{
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Here’s our courses available");
	}; 

	if(req.url === "/addcourse" && req.method === "PUT")
	{
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Add a course to our resources");
	}; 


	if(req.url === "/updatecourse" && req.method === "PATCH")
	{
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update a course to our resources");
	}; 


	if(req.url === "/archivecourses" && req.method === "GET")
	{
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Archive courses to our resources");
	}; 




}).listen(`${port}`);
console.log(`Running at localhost: ${port}`);

		
